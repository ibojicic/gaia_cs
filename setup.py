from setuptools import setup

setup(
    name='GAIA PN CS tools',
    author='Ivan Bojicic',
    author_email='ibojicic@gmail.com',
    license="BSD",

    version='0.1',
    py_modules=['gaia_plots'],
    install_requires=[
        # 'Click',
    ],
    entry_points={
        'console_scripts': [
            'gaia_plots=gaia_plots:cli'
        ]
    }
)
