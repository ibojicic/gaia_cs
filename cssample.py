import inspect
import os
from random import shuffle

import MySQLdb
import numpy as np
import pandas as pd
import qb_astro.physics as myph
import qb_common.dicts as qbdicts
import qb_common.mysqls as qbmysqls
import qb_common.parse as qbparse
import scipy.stats as ss
from astropysics.phot import abs_mag


class CSSample(object):
    __table = None
    __extra_where = None
    __bins = None
    __frozen = None
    __excess = None
    __bins_table = None

    __norm_params = None
    __sysarg = None

    def __init__(self, angdist_limit, maxdiam, mindiam, table, offset=None, shuffled=False, extra_where=None):

        self.angdist_limit = angdist_limit
        self.maxdiam = maxdiam
        self.mindiam = mindiam
        self.mysql_table = table

        self.ini_file = 'default'

        self.set_database().set_cursor()

        self.set_sys_arg()

        self.extra_where = extra_where

        if offset is not None:
            self.extra_where = self.offset_selector(offset)


    def set_bins(self, max_bins, hist_range):
        hist,edges = np.histogram(self.column_values(self.sys_arg('abs_mag')), max_bins, hist_range)
        centers = (edges[0:-1] + edges[1:]) / 2.
        hist_widths = abs(edges[1] - edges[0])
        self.bins = {'hist':hist,'edges':edges, 'centers':centers, 'widths':hist_widths}
        return self

    def set_excess_bins(self, fit_sample):
        excess = self.bins['hist'] - fit_sample.unfreeze(self.bins['centers'])
        self.__bins['excess'] = excess
        return self


    def column_values(self, column):
        if column in self.table.loc[:]:
            return self.table.loc[:, column]
        return []

    def bins_values(self, column):
        if column in self.bins_table.loc[:]:
            return self.bins_table.loc[:, column]
        return []

    def set_bins_table(self):
        if self.bins_table is None:
            self.bins_table = {}
        self.bins_table = qbdicts.extract_keys(self.bins,['hist','centers','excess'])
        return self



    def set_table(self):
        sql = 'SELECT ' \
              'g.`idPNMain`, ' \
              'g.`phot_g_mean_mag`, ' \
              'A4.`D_mean`, ' \
              "A4.`E(B-V)` as 'E_B_V'" \
              ' FROM `ibojicic`.`{}` g LEFT JOIN `MainPNData`.`Frew_2016_tblA4` A4 ' \
              ' ON A4.`idPNMain` = g.`idPNMain` ' \
              'LEFT JOIN `MainGPN`.`GPNFullView` v ON g.`idPNMain` = v.`idPNMain` ' \
              'WHERE v.`PNstat` = \'T\' ' \
              'AND g.`angDist`<{} ' \
              'AND A4.`D_mean` IS NOT NULL ' \
              'AND ((v.`MajDiam` <= {} AND v.`MajDiam` >= {}) OR v.`MajDiam` IS NULL) ' \
              '{};'.format(self.mysql_table, self.angdist_limit, self.maxdiam, self.mindiam, self.and_where)

        # read mysql into pandas table
        self.__table = pd.read_sql(sql, self.database)
        return self

    # --------------- GETTERS/SETTERS ------------------------

    @property
    def bins_table(self):
        return self.__bins_table

    @bins_table.setter
    def bins_table(self,bins_table):
        self.__bins_table = pd.DataFrame(bins_table)

    @property
    def bins(self):
        return self.__bins

    @bins.setter
    def bins(self,bins):
        self.__bins = bins

    @property
    def label(self):
        return self.__label

    @label.setter
    def label(self,label):
        self.__label = label


    @property
    def ini_file(self):
        return self.__ini_file

    @ini_file.setter
    def ini_file(self, file_name):
        if file_name == 'default':
            self.__ini_file = '{}/{}'.format(os.path.dirname(inspect.stack()[0][1]), 'parameters.ini')
        else:
            self.__ini_file = file_name

    @property
    def database(self):
        return self.__database

    def set_database(self):
        self.__database = qbmysqls.hash_database()
        return self

    @property
    def cursor(self):
        return self.__cursor

    def set_cursor(self):
        self.__cursor = self.database.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        return self

    @property
    def angdist_limit(self):
        """
        get angular distance limit (distance between centroid position and gaia source)
        :return: float 
        """
        return self.__angdist_limit

    @angdist_limit.setter
    def angdist_limit(self, angdist_limit):
        """
        get angular distance limit (distance between centroid position and gaia source)
        :param angdist_limit: float
        """
        self.__angdist_limit = angdist_limit

    @property
    def maxdiam(self):
        """
        Get Major Diameter limit
        :return: float 
        """
        return self.__maxdiam

    @maxdiam.setter
    def maxdiam(self, majdiam_limit):
        """
        Set Major Diameter Limit
        :param majdiam_limit: float
        """
        self.__maxdiam = majdiam_limit

    @property
    def mindiam(self):
        """
        Get Major Diameter limit
        :return: float 
        """
        return self.__mindiam

    @mindiam.setter
    def mindiam(self, majdiam_limit):
        """
        Set Major Diameter Limit
        :param majdiam_limit: float
        """
        self.__mindiam = majdiam_limit

    @property
    def mysql_table(self):
        """
        Get MySQL table where the GAIA data is stored
        :return: string
        """
        return self.__mysql_table

    @mysql_table.setter
    def mysql_table(self, table):
        """
        Set MySQL table where the GAIA data is stored
        :param table: string
        """
        self.__mysql_table = table

    @property
    def extra_where(self):
        """
        Get sequence of dictionaries of parameters for MySQL WHERE 
        :return: list
        """
        return self.__extra_where

    @extra_where.setter
    def extra_where(self, where_dict):
        """
        Append a new dictionary of parameters for MySQL WHERE
        :param where_dict: dictionary
        """
        if self.__extra_where is None:
            self.__extra_where = []
        if self.validate_extra_where(where_dict):
            self.__extra_where.append(where_dict)

    @property
    def and_where(self):
        """
        Make WHERE query e.g. g.`some_field` IN (1,2,3) AND b.`other_field` IN (3,4,5)
        :return: string
        """
        result = ''
        if self.extra_where and self.extra_where is not None:
            result = qbmysqls.where_in(self.extra_where)
            if result != '':
                result = 'AND {}'.format(result)
        return result

    @property
    def table(self):
        """
        Get pandas table with objects parameteres
        :return: list
        """
        if self.__table is None:
            self.set_table()
        return self.__table

    @property
    def norm_params(self):
        return self.__norm_params

    @norm_params.setter
    def norm_params(self,norm_params):
        self.__norm_params = norm_params

    @property
    def lognorm_params(self):
        return self.__lognorm_params

    @property
    def frozen(self):
        return self.__frozen

    @frozen.setter
    def frozen(self,function):
        self.__frozen = function

    # ----------- FITTING FUNCTIONS -----------------

    def unfreeze(self, x):
        return self.frozen.pdf(x) * self.norm_params['no']

    def fit_normal(self, multy_factor=1.):
        """
        Fit normal distribution to one column of the table and store it in .norm_parameters 
        dictionary
        :param column: string name of the column
        :param multy_factor: factor to multiply number of objects in the column
        :return: 
        """
        data = self.column_values(self.sys_arg('abs_mag'))
        mu, std = ss.norm.fit(data)
        self.norm_params = {'mu': mu, 'std': std, 'no': len(data) * multy_factor}
        self.frozen = ss.norm(mu,std)
        return self



    def fit_lognormal(self, multy_factor=1.):
        """
        Fit normal distribution to one column of the table and store it in .norm_parameters 
        dictionary
        :param column: string name of the column
        :param multy_factor: factor to multiply number of objects in the column
        :return: 
        """
        data = self.column_values(self.sys_arg('abs_mag'))
        mu, std, snv = ss.lognorm.fit(data)

        self.__lognorm_params = {'mu': mu, 'std': std, 'snv':snv, 'no': len(data) * multy_factor}
        return ss.lognorm(mu,std,snv)

    # ---------- SAMPLE SELECTORS -------------------------

    def set_subsample(self, sample_type):
        if sample_type == ' all':
            pass
        elif sample_type == 'bulge':
            self.extra_where = {
                'tbl': 'g',
                'field': 'idPNMain',
                'in': 'IN',
                'data': self.GB_sql_sample(),
                'type': 'number'
            }
        elif sample_type == 'disk':
            self.extra_where = {
                'tbl': 'g',
                'field': 'idPNMain',
                'in': 'NOT IN',
                'data': self.GB_sql_sample(),
                'type': 'number'
            }
        return self

    def GB_sql_sample(self):
        sql = "SELECT `idPNMain` FROM `MainPNSamples`.`GB_pne`;"
        self.cursor.execute(sql)
        res = list(self.cursor.fetchall())
        return [i['idPNMain'] for i in res]

    # ----------- NEW COLUMNS -------------------

    def set_abs_mag(self, band='g'):
        """
        Insert extra column in the sample with calculated Absolute Magnitudes 
        :return: self
        """
        D_col = self.sys_arg('d_col')
        mag_col = self.sys_arg('mag_col')
        E_B_V_col = self.sys_arg('e_b_v_col')
        abs_mag_col = self.sys_arg('abs_mag')

        extintion_factor = float(self.sys_arg('{}_extinction_factor'.format(band)))
        try:
            self.table.loc[:, abs_mag_col] = \
                myph.extinction_correction_EBV(
                    abs_mag(self.table.loc[:, mag_col], 1E3 * self.table.loc[:, D_col]),
                    self.table.loc[:, E_B_V_col],
                    extintion_factor
                )
        except:
            print "Couldn't calculate absolute magnitudes..."
            exit()

        return self

    def shuffle_parameter(self, parameter):
        """
        Shuffle values in one columns of the sample
        :param parameter: string name of the column
        :return: self
        """
        par_values = self.column_values(parameter)
        shuffle(par_values)
        for key, value in enumerate(par_values):
            self.__table[key][parameter] = value
        return self

    # ------------ VALIDATORS -------------------

    def validate_extra_where(self, extra_where):
        return True

    # ----------- INI FUNCTIONS -------------------

    def set_sys_arg(self):
        self.__sysarg = qbparse.parse_ini_arguments(self.ini_file, 'system')
        return self

    def sys_arg(self, arg):
        return self.__sysarg[arg]

    # ----------- STANDARDS -------------------

    @staticmethod
    def offset_name(prefix, no):
        return "{}_offset_{}".format(prefix, no)

    def offset_selector(self, no):
        return {
            'tbl': 'g',
            'field': 'box',
            'in': 'IN',
            'data': no,
            'type': 'number'
        }
