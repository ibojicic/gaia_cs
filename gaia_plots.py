import click
from cssample import CSSample
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as ss

from qb_ploting.templates import shared_x_1


@click.command()
@click.argument('file_prefix', nargs=1)
@click.option('--sample', '-s', multiple=True, type=click.Choice(
    ['bulge', 'disk', 'all']), default='all')
@click.option('--angdist', '-a', default=5)
@click.option('--maxdiam', '-x', default=300)
@click.option('--mindiam', '-n', default=0)
@click.option('--cumulative', '-c', is_flag=True)
def cli(sample, file_prefix, angdist, maxdiam, mindiam, cumulative):
    histo_range = (-10., 10.)

    # loop trough samples
    for sample_name in sample:
        plt.close('all')

        # main sample
        cs_all = CSSample(angdist, maxdiam, mindiam, 'gaia_test')

        prepare_sample_for_hist(cs_all, sample_name, 'targets')

        # offsets
        cs_offsets = None
        for i in range(1, 9):
            if cs_offsets is None:
                cs_offsets = {}
            offset_sample = CSSample(angdist, maxdiam, mindiam, 'gaia_offsets', i)
            prepare_sample_for_hist(offset_sample, sample_name, 'offset {}'.format(i))
            cs_offsets[offset_sample.offset_name(sample_name, i)] = offset_sample

        # combined offsets for normal distribution estimate
        cs_combined_offsets = CSSample(angdist, maxdiam, mindiam, 'gaia_offsets', None)
        prepare_sample_for_hist(cs_combined_offsets, sample_name, 'full offsets', False)
        cs_combined_offsets.fit_normal(1. / 8.)
        x, p = get_fit(cs_combined_offsets.norm_params, histo_range)

        # shared parameters
        min_bins = find_min_bins(histo_range, cs_all, cs_offsets)

        # target positions
        cs_all.set_bins(min_bins, histo_range)
        cs_all.set_excess_bins(cs_combined_offsets)
        cs_all.set_bins_table()
        hist_width = cs_all.bins['widths']

        # offset positions
        for i in cs_offsets:
            cs_offsets[i].set_bins(min_bins, histo_range)
            cs_offsets[i].set_excess_bins(cs_combined_offsets)
            cs_offsets[i].set_bins_table()

        # plotting
        # ini axes
        ax_histo, ax_excess = shared_x_1('M$_{Gaia\ G}$', 'No', 'No', 0, [3, 2])

        # target positions histogram
        add_bar(ax_histo,cs_all,'hist',hist_width)
        # target position excess
        add_bar(ax_excess,cs_all,'excess',hist_width)

        for i in cs_offsets:
            # offset positions histogram
            add_step(ax_histo,cs_offsets[i],'hist')
            # offset positions excess
            add_step(ax_excess,cs_offsets[i],'excess')

        # fit to the offsets
        ax_histo.plot(x, p, 'k--', linewidth=0.5, label='offsets pdf')

        # legend
        ax_histo.legend(loc='best',frameon=False)

        plt.grid(False)
        plt.savefig(plot_file_name(sample_name, file_prefix, angdist, maxdiam, mindiam))

        # plt.show()


def prepare_sample_for_hist(input_sample, sample_name, label, fit_normal=True):
    input_sample.set_subsample(sample_name)
    input_sample.set_table()
    input_sample.set_abs_mag()
    input_sample.label = label
    if fit_normal:
        input_sample.fit_normal()


def find_min_bins(range, *args):
    min_bins = 1E5
    for css_table in args:
        if isinstance(css_table, dict):
            for i in css_table:
                tmp_max_bins = no_bins(css_table[i].table.loc[:, css_table[i].sys_arg('abs_mag')], range)
        else:
            tmp_max_bins = no_bins(css_table.table.loc[:, css_table.sys_arg('abs_mag')], range)

        if tmp_max_bins < min_bins:
            min_bins = tmp_max_bins
    return min_bins


def no_bins(table, range):
    hist, edges = np.histogram(table, bins='auto', range=range)
    return len(edges) - 1


def get_fit(params, range, cdf=False):
    x = np.linspace(range[0], range[1], 100)
    if cdf:
        p = ss.norm.cdf(x, params['mu'], params['std']) * params['no']
    else:
        p = ss.norm.pdf(x, params['mu'], params['std']) * params['no']
    return x, p


def plot_file_name(sample_name, file_prefix, angdist, maxdiam, mindiam):
    return "{}_s{}_r{}_Dmin{}_Dmax{}.eps".format(file_prefix, sample_name, angdist, mindiam, maxdiam)


def add_bar(axis,css,y_col,hist_width):
    axis.bar(left=css.bins_values('centers'), height=css.bins_values(y_col), align='center',
                 edgecolor='black', facecolor='lightgray', width=hist_width, label=css.label)

def add_step(axis,css,y_col):
    axis.step(x=css.bins_values('centers'), y=css.bins_values(y_col),
                  where='mid', alpha=0.7, linewidth=0.7, label=css.label)
